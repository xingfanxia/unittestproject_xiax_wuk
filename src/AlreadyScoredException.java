/**
 * Created by wuk on 1/11/17.
 */
public class AlreadyScoredException extends RuntimeException {
    public AlreadyScoredException(String message) {
        super(message);
    }
}
