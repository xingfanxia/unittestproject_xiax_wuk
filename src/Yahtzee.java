import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;


/**
 * Created by xingfanxia on 1/10/17.
 */
public class Yahtzee {
    private int[] dice_values;
    public String[][] playList = {{"Yahtzee", "F"},{"FullHouse", "F"},{"ThreeofaKind", "F"},{"LargeStriaght", "F"}};
    public Integer totalScore;

    private Integer generateRandInt() {
        //generate a number between 1 and 6
        int randomNum = ThreadLocalRandom.current().nextInt(1, 6 + 1);
        return randomNum;
    }

    private void roll_dices() {
        for (int i = 0; i < 5; i++) {
            dice_values[i] = generateRandInt();
        }
    }

    public Yahtzee () {
        dice_values = new int[5];
        totalScore = 0;
        roll_dices();
    }

    public void feed_dices(int[] dices) {
        dice_values = dices;
    }
    public int[] get_dice_faces() {
        return dice_values;
    }

    public Integer getTotalScore() {
        return totalScore;
    }
}
