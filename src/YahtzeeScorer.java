import java.util.Arrays;

/**
 * Created by xingfanxia on 1/10/17.
 */
public class YahtzeeScorer {
    private Integer YahtzeeScore = 50;
    private Integer LargeStriaightScore = 40;
    private Integer FullhouseScore = 25;

    private Integer ChoiceYahtzee = 0;
    private Integer ChoiceFullhouse = 1;
    private Integer ChoiceThreeofaKind = 2;
    private Integer ChoiceLargeStraight = 3;
    public YahtzeeScorer() {

    }

    public Integer scoreYahtzee(int[] dices) {
        int[] counts = new int[6];
        for (int i = 0; i < dices.length; i++) {
            counts[dices[i]-1]++;
        }

        for (int i : counts) {
            if (i==5) {
                return YahtzeeScore;
            }
        }

        return 0;
    }

    public Integer scoreFullHouse(int[] dices) {
        int[] counts = new int[6];
        for (int i = 0; i < dices.length; i++) {
            counts[dices[i]-1]++;
        }
        boolean flag_pair = false;
        boolean flag_triple = false;

        for (int i : counts) {
            flag_pair |= (i==2);
            flag_triple |= (i==3);
            if (i==5) {
                return FullhouseScore;
            }
        }

        if (flag_pair && flag_triple) {
            return FullhouseScore;
        }
        return 0;
    }

    public Integer scoreThreeofaKind(int[] dices) {
        Arrays.sort(dices);

        for (int i = dices.length -1; i > 1; i--) {
            if (dices[i] == dices[i-1] && dices[i] == dices[i-2]) {
                Integer toAdd = dices[i] + dices[i-1] + dices[i-2];
                return toAdd;
            }
        }

        return 0;
    }


    public Integer scoreLargeStriaght(int[] dices) {
        int[] counts = new int[6];
        //only two valid Large Straights
        int[] valid_1 = new int[]{0, 1, 1, 1, 1, 1};
        int[] valid_2 = new int[]{1, 1, 1, 1, 1, 0};

        for (int i = 0; i < dices.length; i++) {
            counts[dices[i]-1]++;
        }
        if (Arrays.equals(counts, valid_1) || Arrays.equals(counts, valid_2) ) {
            return LargeStriaightScore;
        }
        return 0;
    }

    public Integer check(Yahtzee yz, Integer choice) {
        int[] dices = yz.get_dice_faces();
        switch (choice) {
            case 0:
                System.out.println("You choose Yahtzee!");

                if (yz.playList[0][1] != "T") { // if Yahtzee is never used!
                    yz.playList[0][1] = "T";
                    Integer score = scoreYahtzee(dices);
                    if (score == YahtzeeScore) {
                        System.out.format("You Scored %d ", score);
                        return score;
                    } else {
                        System.out.format("You didn't get any score!");
                        return 0;
                    }
                } else {
                    System.out.println("You have already used Yahtzee before!!");
                    return -1;
                }
            case 1:
                System.out.println("You choose Fullhouse!");

                if (yz.playList[1][1] != "T") { // if Fullhouse is never used!
                    yz.playList[1][1] = "T";
                    Integer score = scoreFullHouse(dices);
                    if (score == FullhouseScore) {
                        System.out.format("You Scored %d", score);
                        return score;
                    } else {
                        System.out.format("You didn't get any score!");
                        return 0;
                    }
                } else {
                    System.out.println("You have already used Fullhouse before!!");
                    return -1;
                }
            case 2:
                System.out.println("You choose ThreeofaKind!");

                if (yz.playList[2][1] != "T") { // if ThreeofaKind is never used!
                    yz.playList[2][1] = "T";
                    Integer score = scoreThreeofaKind(dices);
                    if (score != 0) {
                        System.out.format("You Scored %d", score);
                        return score;
                    } else {
                        System.out.format("You didn't get any score!");
                        return 0;
                    }
                } else {
                    System.out.println("You have already used ThreeofaKind before!!");
                    return -1;
                }
            case 3:
                System.out.println("You choose LargeStraight!");

                if (yz.playList[3][1] != "T") { // if LargeStraight is never used!
                    yz.playList[3][1] = "T";
                    Integer score = scoreLargeStriaght(dices);
                    if (score == LargeStriaightScore) {
                        System.out.format("You Scored %d", score);
                        return score;
                    } else {
                        System.out.format("You didn't get any score!");
                        return 0;
                    }
                } else {
                    System.out.println("You have already used LargeStraight before!!");
                    return -1;
                }
        }
        return -1;
    }
}
