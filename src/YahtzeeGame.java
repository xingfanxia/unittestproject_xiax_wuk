/**
 * Created by wuk on 1/11/17.
 */
import java.util.Arrays;

public class YahtzeeGame {
    public Yahtzee yz;
    public YahtzeeScorer ys;

    public YahtzeeGame() {
        ys = new YahtzeeScorer();
    }

    public void play(Yahtzee yz, Integer choice){
        System.out.println("\n 0 for Yahtzee, 1 for Fullhouse, 2 for ThreeofAkind" +
                "3 for LargeStraight");
        Integer score = ys.check(yz, choice);
        if (score != -1) {
            yz.totalScore += score;
        } else {
            throw new AlreadyScoredException("This is already used");
        }
    }
}
