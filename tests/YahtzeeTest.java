import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by xingfanxia on 1/10/17.
 */
public class YahtzeeTest {
    private Yahtzee yz;
    private YahtzeeScorer ys;
    private YahtzeeGame yg;

    @Before
    public void setUp() {
        yg = new YahtzeeGame();
        yz = new Yahtzee();
        ys = new YahtzeeScorer();
    }

//    @Test
//    public void canGenerateRandom() {
//        int counter = 1;
//        while (counter <= 100) {
//            int num = yz.generateRandInt();
//            assertTrue(1 <= num && num <= 6);
//            counter++;
//        }
//    }

    @Test
    public void five_dices() {
        int[] dices = yz.get_dice_faces();
        //There are five dices
        assertEquals(5, dices.length);
    }

    @Test
    public void dices_in_range() {
        int[] dices = yz.get_dice_faces();
        for (Integer i : dices) {
            //each dice has a face between 1 and 6
            assertTrue(1 <= i && i <= 6);
        }
    }

    @Test
    public void canFeedDices() {
        int[] test_dices = new int[]{1, 1, 1, 1, 1};
        yz.feed_dices(test_dices);
        assertEquals(test_dices, yz.get_dice_faces());
    }

    @Test
    public void scoreYahtzeeWorks() {
        int[] test_dices = new int[]{1, 1, 1, 1, 1};
        Integer score = ys.scoreYahtzee(test_dices);
        assertEquals((Integer)50, score);
    }

    @Test
    public void scoreFullhouseWorks() {
        int[] test_dices = new int[]{1, 1, 1, 2, 2};
        Integer score = ys.scoreFullHouse(test_dices);
        assertEquals((Integer)25, score);
    }

    @Test
    public void scoreThreeofaKindWorks() {
        //more tests should be here?
        int[] test_dices = new int[]{3, 3, 3, 4, 5};
        Integer score = ys.scoreThreeofaKind(test_dices);
        assertEquals((Integer)9, score);

        test_dices = new int[]{6, 3, 6, 6, 5};
        score = ys.scoreThreeofaKind(test_dices);
        assertEquals((Integer)18, score);
    }

    @Test
    public void scoreLargeStraightWorks() {
        //more tests should be here?
        int[] test_dices = new int[]{2, 1, 4, 3, 5};
        Integer score = ys.scoreLargeStriaght(test_dices);
        assertEquals((Integer)40, score);

        test_dices = new int[]{1, 3, 2, 5, 4};
        score = ys.scoreLargeStriaght(test_dices);
        assertEquals((Integer)40, score);
    }

    @Test
    public void canCheckYahtzee() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 1, 1, 1};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 0);
        assertEquals((Integer)50, score);
    }

    @Test
    public void canCheckFullhouse() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 1, 2, 2};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 1);
        assertEquals((Integer)25, score);
    }

    @Test
    public void canCheckFullhouseWorkYahtzee() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 1, 1, 1};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 1);
        assertEquals((Integer)25, score);
    }


    @Test
    public void canCheckThreeOfaKind() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 1, 5, 4};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 2);
        assertEquals((Integer)3, score);
    }

    @Test
    public void canCheckLargeStraight() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{2, 1, 4, 3, 5};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 3);
        assertEquals((Integer)40, score);
    }

    @Test
    public void canChoosePlay() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{2, 6, 4, 3, 5};
        new_yz.feed_dices(test_dices);
        yg.play(new_yz, 3);
    }

    @Test
    public void AlreadyScoredLargeStraight() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{2, 6, 4, 3, 5};
        new_yz.feed_dices(test_dices);
        yg.play(new_yz, 3);
        assertEquals((Integer)40, new_yz.getTotalScore());

        try {
            test_dices = new int[]{2, 6, 4, 3, 5};
            new_yz.feed_dices(test_dices);
            yg.play(new_yz, 3);
        } catch (AlreadyScoredException sucess){
            // If we caught it, that's what we want! Continue to success!
        }
    }

    @Test
    public void AlreadyScoredYahtzee() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 1, 1, 1};
        new_yz.feed_dices(test_dices);
        yg.play(new_yz, 0);
        assertEquals((Integer)50, new_yz.getTotalScore());

        try {
            test_dices = new int[]{1, 1, 1, 1, 1};
            new_yz.feed_dices(test_dices);
            yg.play(new_yz, 0);
        } catch (AlreadyScoredException sucess){
            // If we caught it, that's what we want! Continue to success!
        }
    }

    @Test
    public void AlreadyScoredFullhouse() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{2, 2, 2, 3, 3};
        new_yz.feed_dices(test_dices);
        yg.play(new_yz, 1);
        assertEquals((Integer)25, new_yz.getTotalScore());

        try {
            test_dices = new int[]{2, 2, 2, 3, 3};
            new_yz.feed_dices(test_dices);
            yg.play(new_yz, 1);
        } catch (AlreadyScoredException sucess){
            // If we caught it, that's what we want! Continue to success!
        }
    }

    @Test
    public void AlreadyScoredThreeofaKind() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{2, 2, 2, 3, 5};
        new_yz.feed_dices(test_dices);
        yg.play(new_yz, 2);
        assertEquals((Integer)6, new_yz.getTotalScore());

        try {
            test_dices = new int[]{2, 2, 2, 3, 5};
            new_yz.feed_dices(test_dices);
            yg.play(new_yz, 2);
        } catch (AlreadyScoredException sucess){
            // If we caught it, that's what we want! Continue to success!
        }
    }

    @Test
    public void canCheckFailYahtzee() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 3, 1, 1};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 0);
        assertEquals((Integer)0, score);
    }

    @Test
    public void canCheckFailFullhouse() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 5, 2, 2};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 1);
        assertEquals((Integer)0, score);
    }

    @Test
    public void canCheckFailThreeOfaKind() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 2, 5, 4};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 2);
        assertEquals((Integer)0, score);
    }

    @Test
    public void canCheckFailLargeStraight() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{2, 1, 6, 3, 5};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 3);
        assertEquals((Integer)0, score);
    }

    @Test
    public void canChecknotValidChoice() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{2, 1, 6, 3, 5};
        new_yz.feed_dices(test_dices);
        Integer score = ys.check(new_yz, 9);
        assertEquals((Integer) (-1), score);
    }

    @Test
    public void getCurrentScore() {
        Yahtzee new_yz = new Yahtzee();
        int[] test_dices = new int[]{1, 1, 1, 1, 1};
        new_yz.feed_dices(test_dices);
        yg.play(new_yz, 0);

        test_dices = new int[]{2, 2, 2, 3, 3};
        new_yz.feed_dices(test_dices);
        yg.play(new_yz, 1);
        assertEquals((Integer)75, new_yz.getTotalScore());
    }
}
